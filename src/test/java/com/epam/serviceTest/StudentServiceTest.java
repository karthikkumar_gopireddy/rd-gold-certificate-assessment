package com.epam.serviceTest;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.AssociateDto;
import com.epam.dto.BatchDto;
import com.epam.exception.EmailAlreadyExistsException;
import com.epam.exception.ResourceNotFoundException;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.AssociateService;

@ExtendWith(MockitoExtension.class)
 class StudentServiceTest {
	@Mock
	private AssociateRepository studentRepository;
	@Mock
	private BatchRepository batchRepository;

	@Mock
	private ModelMapper modelMapper;

	@InjectMocks
	private AssociateService studentService;

	private Batch batch;
	private BatchDto batchDto;
	private AssociateDto associateDto;
	private List<AssociateDto> associateDtos;
	private Date date;
	private List<Associate> associates;
    private Associate associate;
	@BeforeEach
	public void setUp() {
		date = new Date(2017, 06, 23, 12, 39, 20);
		batch = new Batch(1, "rt", "pp", date, date, associates);
		batchDto = new BatchDto("string", "string", date, date);
		associateDto = new AssociateDto("fg", "r@gmail.com", "M", "pp", "ACTIVE", batchDto);
		associate = new Associate(0, "kl", "d@gmail.com", "pp", "l", "o", batch);
		associates = new ArrayList<>();
		associates.add(associate);
		associateDtos = new ArrayList<>();
		associateDtos.add(new AssociateDto("", null, null, null, null, batchDto));

	}

	@Test
	void addStudent1() {
		Mockito.when(batchRepository.findByBatchname(associateDto.getBatch().getBatchname())).thenReturn(Optional.of(batch));
		Mockito.when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(studentRepository.save(associate)).thenReturn(associate);
		studentService.createAssociate(associateDto);
		Mockito.verify(studentRepository).save(associate);
	}
	
	@Test
	void addStudent2() {
		Mockito.when(batchRepository.findByBatchname(associateDto.getBatch().getBatchname())).thenReturn(Optional.empty());
		Mockito.when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(studentRepository.save(associate)).thenReturn(associate);
		studentService.createAssociate(associateDto);
		Mockito.verify(studentRepository).save(associate);
	}

	@Test
	void addStudent3() {
		Mockito.when(studentRepository.findByEmail(associateDto.getEmail())).thenReturn(Optional.of(associate));
		assertThrows(EmailAlreadyExistsException.class, () -> studentService.createAssociate(associateDto));
	}

	@Test
	void getBatch2() {
		Mockito.when(studentRepository.findById(1)).thenReturn(Optional.of(associate));
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		studentService.getAssociate(1);
	}
	@Test
	void getBatch1() {
		Mockito.when(studentRepository.findById(1)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> studentService.getAssociate(1));
	}

	@Test
	void getAllStudent() {
		Mockito.when(studentRepository.findAll()).thenReturn(associates);
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		studentService.getAllAssociate();
	}

	@Test
	void updateBatch1() {
		Mockito.when(studentRepository.findByEmail("d@gmail.com")).thenReturn(Optional.of(associate));
		Mockito.when(studentRepository.save(associate)).thenReturn(associate);
		studentService.updateAssociate("d@gmail.com", associateDto);
	}

	@Test
	void updateBatch2() {
		Mockito.when(studentRepository.findByEmail("d@gmail.com")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> studentService.updateAssociate("d@gmail.com",associateDto));
	}

	@Test
	void deleteBatch1() {
		Mockito.when(studentRepository.findById(0)).thenReturn(Optional.of(associate));
		Mockito.doNothing().when(studentRepository).delete(associate);
		studentService.deleteAssociate(associate.getId());
	}

	@Test
	void deleteBatch2() {
		Mockito.when(studentRepository.findById(0)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> studentService.deleteAssociate(associate.getId()));

	}
}
