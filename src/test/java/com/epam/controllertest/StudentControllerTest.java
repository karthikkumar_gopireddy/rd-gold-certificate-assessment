package com.epam.controllertest;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.epam.controller.AssociateController;
import com.epam.dto.BatchDto;
import com.epam.dto.AssociateDto;
import com.epam.model.Batch;
import com.epam.model.Associate;
import com.epam.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class StudentControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private AssociateService studentService;
	private Batch batch;
	private BatchDto batchDto;
	private AssociateDto associateDto;
	private List<AssociateDto> associateDtos;
	private Date date;
	private List<Associate> associates;

	@BeforeEach
	public void setUp() {
		date = new Date(2017, 06, 23, 12, 39, 20);
		batch = new Batch(1, "rt", "pp", date, date, associates);
		batchDto = new BatchDto("string", "string", date, date);
		associateDto = new AssociateDto("fg", "r@gmail.com", "M", "pp", "ACTIVE", batchDto);
		Associate associate = new Associate(0, "kl", "d@gmail.com", "pp", "l", "o", batch);
		associates = new ArrayList<>();
		associates.add(associate);

		associateDtos = new ArrayList<>();
		associateDtos.add(new AssociateDto("", null, null, null, null, batchDto));

	}

	@Test
	void createStudent() throws Exception {
		Mockito.when(studentService.createAssociate(associateDto)).thenReturn(associateDto);
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(associateDto);
		mockMvc.perform(post("/associate/add").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(status().isCreated());
	}

	@Test
	void validateStudent1() throws Exception {
		Mockito.when(studentService.createAssociate(any(AssociateDto.class))).thenThrow(RuntimeException.class);
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(associateDto);
		mockMvc.perform(post("/associate/add").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(status().isInternalServerError());
	}

	@Test
	void validateStudent2() throws Exception {
		 AssociateDto associateDto = new AssociateDto();

		mockMvc.perform(post("/associate/add").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());
	}



	@Test
	void viewBatch() throws Exception {
		Mockito.when(studentService.getAssociate(1)).thenReturn(associateDto);
		mockMvc.perform(get("/associate/{id}", "1")).andExpect(status().isOk());
	}

	@Test
	void viewAllAssociate() throws Exception {
		Mockito.when(studentService.getAllAssociate()).thenReturn(associateDtos);
		mockMvc.perform(get("/associate/allUsers")).andExpect(status().isOk());
	}

	@Test
	void updateBatch() throws Exception {
		Mockito.when(studentService.updateAssociate("r@gmail.com", associateDto)).thenReturn(associateDto);
		ObjectMapper map = new ObjectMapper();
		String json = map.writeValueAsString(associateDto);
		mockMvc.perform(
				put("/associate/update/{email}", "r@gmail.com").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());
	}

	@Test
	void removeBook() throws Exception {
		Mockito.doNothing().when(studentService).deleteAssociate(0);
		mockMvc.perform(delete("/associate/delete/{id}", "1")).andExpect(status().isOk());
	}
}
