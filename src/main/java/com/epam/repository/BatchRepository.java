package com.epam.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.model.Batch;

public interface BatchRepository extends JpaRepository<Batch,Integer>{
	 Optional<Batch> findByBatchname(String batchname);
}
