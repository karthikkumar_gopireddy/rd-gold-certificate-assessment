package com.epam.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.model.Associate;

public interface AssociateRepository extends JpaRepository<Associate,Integer> {
    List<Associate> findByBatchId(int batchId);
    Optional<Associate> findByEmail(String email);
}
