package com.epam.model;

import java.util.Date;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Batch {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	String batchname;
	String practice;
	Date startDate;
	Date endDate;
	@OneToMany(mappedBy = "batch", cascade = CascadeType.ALL)
	List<Associate>  associate;
	public void setId(int id) {
		this.id = id;
	}
	public void setBatchname(String batchname) {
		this.batchname = batchname;
	}
	public void setPractice(String practice) {
		this.practice = practice;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public void setAssociate(List<Associate> associate) {
		associate.forEach(n->
			n.setBatch(this));
		this.associate = associate;
	}
	
	
}
