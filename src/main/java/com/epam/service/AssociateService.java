package com.epam.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.AssociateDto;
import com.epam.exception.EmailAlreadyExistsException;
import com.epam.exception.ResourceNotFoundException;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AssociateService {
	@Autowired
	ModelMapper modelMapper;
	@Autowired
	AssociateRepository associateRepository;
	@Autowired
	BatchRepository batchRepository;

	public AssociateDto createAssociate(AssociateDto associateDto) {
		Optional<Associate> user = associateRepository.findByEmail(associateDto.getEmail());
		if (user.isPresent()) {
			log.error("Service Layer, Email Already Exists for this  User");
			throw new EmailAlreadyExistsException("Email Already Exists for User");
		}

		Associate associate = modelMapper.map(associateDto, Associate.class);
		Optional<Batch> batch = batchRepository.findByBatchname(associateDto.getBatch().getBatchname());
		if (batch.isPresent()) {
			associate.setBatch(batch.get());
			log.info("Service layer user details saved sucessfully !!");
			associateRepository.save(associate);
		} else {
			log.info("Service laryer user details saved sucessfully !!");
			associateRepository.save(associate);
		}
		return modelMapper.map(associate, AssociateDto.class);
	}

	public AssociateDto getAssociate(int id) {
		Associate student = associateRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("cannot found resource with this id"));
		log.info("Service Layer,Retrived Associate Details sucessfully !!");
		return modelMapper.map(student, AssociateDto.class);
	}

	public List<AssociateDto> getAllAssociate() {
		List<Associate> student = associateRepository.findAll();
		log.info("Service Layer,Retrived All Associates Details sucessfully !!");
		return student.stream().map(user -> modelMapper.map(user, AssociateDto.class)).toList();
	}

	public AssociateDto updateAssociate(String email, AssociateDto associateDto) {
		Associate associate = associateRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFoundException("cannot found resource with this email"));
		associate.setBatch(null);
		associate.setCollege(associateDto.getCollege());
		associate.setGender(associateDto.getGender());
		associate.setStatus(associateDto.getStatus());
		associate.setGender(associateDto.getGender());
		associate.setEmail(associateDto.getEmail());
		associate.setName(associateDto.getName());
		associateRepository.save(associate);
		log.info("Service Layer, User details updated sucessfully !!");
		return modelMapper.map(associate, AssociateDto.class);
	}

	public void deleteAssociate(int id) {
		Associate student = associateRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("cannot found resource with this id"));
		student.setBatch(null);
		associateRepository.delete(student);
		log.info("Service Layer,Deleted User details sucessfully !!");
	}

}
