package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDto;
import com.epam.service.AssociateService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("associate")
public class AssociateController {
	@Autowired
	AssociateService associateService; 
	@PostMapping("/add")
	public ResponseEntity<AssociateDto> addAssociate(@RequestBody @Valid AssociateDto associateDto) {
		AssociateDto associatedto =associateService.createAssociate(associateDto);
		log.info("Added Sucessfully !!");
		return new ResponseEntity<>(associatedto,HttpStatus.CREATED);
	}

	@GetMapping("{id}")
	public ResponseEntity<AssociateDto> getAssociate(@PathVariable int id) {
		AssociateDto associateDto = associateService.getAssociate(id);
		log.info("Retrived Associate Details Sucessfully !!");
		return new ResponseEntity<>(associateDto,HttpStatus.OK);
	}
	
	@GetMapping("/allUsers")
	public ResponseEntity<List<AssociateDto>> getAllAssociates() {
		log.info("Retrived All Associates Details Sucessfully !!");
		return new ResponseEntity<>(associateService.getAllAssociate(),HttpStatus.OK);
	}
	
	@PutMapping("/update/{email}")
	public ResponseEntity<AssociateDto> updateAssociate(@PathVariable String email  ,@RequestBody @Valid AssociateDto associateDto) {
		AssociateDto associatedto = associateService.updateAssociate(email,associateDto);
		return new ResponseEntity<>(associatedto,HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deleteAssociate(@PathVariable int id) {
		associateService.deleteAssociate(id);
		log.info("Deleted Associate Details !!");
		return new ResponseEntity<>("user deleted !",HttpStatus.OK);
	}

}
