package com.epam.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssociateDto {
	@NotBlank(message = "name should not be empty")
	String name;
	@Email(message = "email should be proper")
	@NotBlank(message = "email should not be empty")
	String email;
	@Pattern(regexp = "M|F")
	@NotBlank(message = "gender should not be empty")
	String gender;
    @NotBlank(message = "college name should not be empty")
	String college;
    @Pattern(regexp = "ACTIVE|INACTIVE")
    @NotBlank(message = "status name should not be empty")
	String status;
    BatchDto batch;
}
