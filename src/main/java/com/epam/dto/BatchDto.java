package com.epam.dto;

import java.util.Date;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BatchDto {
	//int id;
	@NotBlank(message = " batchname should not be empty")
	String batchname;
	@NotBlank(message = "practice name should not be empty")
	String practice;
	Date startDate;
	Date endDate;
//	@NotEmpty(message = "list should not be empty")
//	List<AssociateDto> associate;
	
}
